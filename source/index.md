
# About us

We are a non-profit research institute dedicated to fostering collaboration among central banks and institutions responsible for financial stability. Our mission is to create a dynamic platform for collaboration, the sharing of experiences, and the dissemination of practical, cutting-edge tools for macroprudential modeling and policy analysis. Furthermore, we assist central banks in developing policy-relevant macroprudential models and integrating these models into their policy-making processes, enhancing their capacity to address financial stability challenges effectively.


#### :fontawesome-solid-user-group: Connecting people

> GIMM strives to be a nexus for practitioners grappling with real-world financial challenges. We complement existing platforms by focusing on experience sharing, connecting professionals across institutions, and providing access to state-of-the-art tools. Our workshops are particularly valuable for experts tasked with generating regular analyses for policymakers, including stress scenarios and evaluations of macroprudential policies.


#### :fontawesome-solid-cube: Building macroprudential modeling tools

> GIMM has developed a robust modeling framework for analyzing macro-financial linkages, financial stability, and macroprudential policy. This framework features two-way endogenous interactions between the macro and financial systems, allowing users to examine the financial system holistically and understand the big-picture interactions with the macroeconomy. Its flexibility enables analysis of diverse financial stability and policy scenarios, as well as assessment of macroeconomic impacts on the financial sector.
