--- 
title: GIMM 2025 Central Asia Workshop on Macroprudential Modeling
---

# :fontawesome-solid-pen: GIMM 2025 Central Asia Workshop on Macroprudential Modeling

<!-- [Download workshop leaflet](../assets/GIMM_2024_parameterization_workshop.pdf) -->

<!-- ### Registration

[Register here.](https://forms.monday.com/forms/327577ad57438332559171cc16d66398?r=use1) -->

### Format
* Lectures combined with hands-on simulation exercises

### Content

* General principles of macroprudential analysis using structural models, inherent difficulties, practical issues
* Introduction to a practical macroprudential modeling:: basic overview, model blocks, key principles, use cases
* Modeling macroprudential policies (capital-based policies, credit caps, LTVs,DSTIs), estimating costs and benefits of macroprudential policies 
* Role of macroprudential policy in smoothing financial cycle
* Climate-related risks: classification, modeling, available resources
* Data-based simulations: linking the model to data, top-down stress tests
* Climate-related risk scenarios


### Benefits

Participants received the following:

* Complete modeling framework - equations, documentation, understanding of key transmission channels
* Commented codes - model files, simulation files, data files, reporting files
* Presentations


### When, where

**Dates:** To be announced

**Place:** To be announced

**Charge** We are currently negotiating with a sponsor to make participation free of charge. Details to be announced.

