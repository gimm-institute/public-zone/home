---
title: Macrofinancial stability model
---

# :fontawesome-solid-cube:  Macrofinancial stability model

GIMM has developed an innovative in-house modeling framework specifically designed for medium-term dynamic macro-financial analysis. This framework incorporates two-way macro-financial interactions, key nonlinearities, and a flexible array of potential macroprudential policies.

Built upon insights from large-scale DSGE models like [MAPMOD](https://www.imf.org/en/Publications/WP/Issues/2016/12/31/Financial-Crises-in-DSGE-Models-Selected-Applications-of-MAPMOD-41466), the framework prioritizes simplicity, flexibility, and operational efficiency. It consists of modular components that can be easily added, removed, or adjusted to suit specific needs.

A detailed working paper describing the framework and its use cases is available [here](https://www.cerge-ei.cz/pdf/wp/Wp746.pdf).

Crucially, the framework incorporates nonlinearities essential for macroprudential modeling and policy analysis. This allows us to simulate a wide range of policy interventions, not just theoretical "shock-minus-control" simulations starting from equilibrium. We can also build simulations based on the current state of the economy and existing macroeconomic forecasts.

![Model Overview](../assets/model_overview.png)

**Use cases:**

 * **Policy intervention simulations:** Explore how specific policies affect the financial sector and broader economy through simulations. Gain insights into potential outcomes before implementation.
* **Data-driven projections:** Generate scenarios for future economic and financial conditions based on current trends and forecasts (like central bank projections). Understand potential trajectories based on real-world data.

 * **Shock-minus-control scenarios:** Analyze the impact of unexpected events (shocks) on the economy and financial sector compared to a "business-as-usual" baseline. Assess potential risks and their ripple effects.

 * **Steady-state analysis:** Compare two economic states to understand the impact of changes in underlying factors (parameters) and the path leading from one state to another. Evaluate how long-term policy adjustments might evolve.

**Continuous development:** We remain committed to updating and enhancing the framework with new features and capabilities.


