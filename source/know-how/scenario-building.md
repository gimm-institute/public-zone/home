---
title: Scenario building techniques
---

# :fontawesome-solid-screwdriver-wrench:  Scenario building techniques

We have developed the "delta method" as an easy way to build policy-relevant scenarios. 

The delta method is a simple technique to construct a macrofinancial
scenario (e.g.~a stress scenario or a policy scenario) on top of a
baseline scenario. Most commonly, the baseline scenario is obtained by
conditioning on a baseline macroeconomic forecast. We then add additional
assumptions (unexpected decline of foreign demand, sudden exchange rate
depreciation, or rapid worsening of credit performance for
idiosyncratic reasons, etc.). After applying the delta method, we obtain the
paths of variables in the real and financial sectors that are based on
the baseline scenario but deviate consistently with the additional
assumptions.
