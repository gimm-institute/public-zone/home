--- 
title: Africa 2023 Modeling Workshop
---

# :fontawesome-solid-pen: Africa-focused Hands-on Modeling Workshop

**We would like to thank all participants for their contributions as well as great company!**

### Format
* Lectures combined with hands-on simulation exercises

### Content

* Current issues in model-based macroprudential policy analysis
* Central principles of macroprudential modeling - nonlinearity, assymetry, stock-flow consistency
* Introduction GIMM macroprudential modeling framework: equations, transimission mechanisms, key features
* Generating top-down stress-testing scenarios based on external scenario inputs
* Generating and analyzing financial cycles 
* Representing climate-related risks as scenarios in the model
* Implementation of macroprudential policies in the model
* Role of macroprudential policies in smoothing financial cycles


### When, where

**Dates:** July 17-20, 2023


**Place:** Kigali, Rwanda
