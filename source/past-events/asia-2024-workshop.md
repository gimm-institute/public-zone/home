--- 
title: Asia Spring 2024 Modeling Workshop
---

# :fontawesome-solid-pen: Hands-on Macroprudential Modeling Workshop for Asian Region
**We extend our gratitude to the Asian Development Bank (ADB) and the Bangko Sentral ng Pilipinas (BSP) for their generous hosting of the workshop.**
![Group photo](../assets/ADB_2024_HQ_AJ_DSC_0428.jpg) 

### Format
* Lectures combined with hands-on simulation exercises

### Content

* General principles of macroprudential analysis using structural models, inherent difficulties, practical issues
* Introduction to a practical macroprudential modeling:: basic overview, model blocks, key principles, use cases
* Modeling macroprudential policies (capital-based policies, credit caps, LTVs,DSTIs), estimating costs and benefits of macroprudential policies 
* Role of macroprudential policy in smoothing financial cycle
* Climate-related risks: classification, modeling, available resources
* Data-based simulations: linking the model to data, top-down stress tests
* Climate-related risk scenarios


### Benefits

Participants received the following:

* Complete modeling framework - equations, documentation, understanding of key transmission channels
* Commented codes - model files, simulation files, data files, reporting files
* Presentations


### When, where

**Dates:** February 12 - 16, 2024, 5-day event

**Place:** Manila, Philippiness
