--- 
title: Prague 2024 Workshop on Parameterizing Macroprudential Models
---

# :fontawesome-solid-pen: Workshop on Parameterizing Macroprudential Models

[Download workshop leaflet](../assets/GIMM_2024_parameterization_workshop.pdf)


### Format

* Lectures accompanied by simulation exercises

### Content
This workshop equipped participants with the knowledge and tools to effectively
parameterize macroprudential models. Through a combination of lectures,
discussions, and hands-on exercises, participants gained a deep understanding of:

* The inherent difficulties associated with calibrating macroprudential models.
* Calibration versus estimation: When to use each approach and the pitfalls of traditional methods.
* Common calibration strategies and their applications: Explore effective techniques tailored for macroprudential models.
* Addressing unique model features: Learn how to handle nonlinearities and asymmetries during calibration.
* Model verification and "smell tests": Ensuring your model is well-calibrated and reflects real-world behavior.
* Data requirements for successful calibration: Identify the necessary data types and considerations.

The workshop was based on the in-house GIMM modeling framework implemented in Python.


### Benefits

Participants received the following:

* Complete modeling framework - equations, documentation, understanding of key transmission channels
* Commented codes - model files, simulation files, data files, reporting files
* Presentations


### When, where

**Dates:** October 7-10, 2024, 4 day event

**Place:** Prague, Czech Republic



