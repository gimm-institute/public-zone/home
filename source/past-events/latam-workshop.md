--- 
title: Latin America 2023 Modeling Workshop
---

# :fontawesome-solid-pen: March 2023 Hands-on Modeling Workshop in Colombia

The workshop was organized with kind support of the Bilateral Assistance and Capacity Building Program for Central Banks (BCC) funded by the Swiss State Secretariat for Economic Affairs (SECO).

**We would like to thank all participants for their contributions as well as great company!**
![Group photo](../assets/202303_bogota_workshop.jpg) 

### Format
* Lectures combined with hands-on simulation exercises

### Content

* Central principles of macroprudential modeling - nonlinearity, assymetry, stock-flow consistency
* Introduction GIMM macroprudential modeling framework: equations, transimission mechanisms, key features
* Generating top-down stress-testing scenarios based on external scenario inputs
* Generating and analyzing financial cycles 
* Implementation of macroprudential policies in the model
* Role of macroprudential policies in smoothing financial cycles


### Benefits

Participants received the following:

* Complete modeling framework - equations, documentation, understanding of key transmission channels
* Commented codes - model files, simulation files, data files, reporting files
* Presentations

 
### Detailed workshop content


??? info "Area 1: Practical aspects of macroprudential policy" 

    TOPIC | CONTENT
    ---|---
    **Principles of macroprudential modeling**    | What is different in macroprudential modeling? <br> Role of nonlinearities <br> Macro-financial feedback loops <br> Modeling macroprudentail policies <br> Modularity of the framework
    **Risk identification**                       | Monitoring risks in financial system <br> Limits to our risk monitoring abilities <br> Implications for policy
    **Climate risk**                              | Climate risk in modeling frameworks <br> Ways to implement scenarios on climate risk


??? info "Area 2: Introduction into GIMM macroprudential modeling framework" 

    TOPIC | CONTENT
    ---|---
    **Framework introduction**                    | Purpose, use cases <br> Overview of the key blocks <br> Overview of nonlinearities and feedback loops
    **Banking sector**                            | Banking sector balance sheet <br> Time evolution of a loan portfolio <br> Credit risk and credit performance <br> Portfolio segmentation
    **Credit risk**                               | Credit risk, link to macro <br> Credit risk and allowances <br> Role of nonlinearities
    *Simulation: Credit performance shock*        | Basic shock to credit performance, impact on bank balance sheet
    **Interest rate setting**                     | Stock-flow dynamics <br> Forward-looking cost-plus pricing <br> Credit risk and interest rates <br> Price vs non-price lending conditions
    *Simulation: Credit performance shock, cont.* | Impact on interest rates, lending conditions
    **Credit creation**                           | Link between macro and credit <br> Lending conditions <br> Demand for new credit <br> Credit market equilibrium <br> Deleveraging - flows vs stocks 
    *Simulation: Credit performance shock, cont.* | Impact on credit creation
    **Bank capital**                              | Bank capital accumulation <br> Key P&L items <br> Bank dividends, recapitalization <br> Bank behavior under stress
    *Simulation: Credit performance shock, cont.* | Impact on bank P&L, capital position
    **Macro-financial feedbacks**                 | Linking the financial sector to macroeconomy <br> Negative feedback loops <br> Role of nonlinearities
    *Simulation: Credit performance shock, cont.* | Impact on macroeconomy <br> Role of nonlinearities
    **Model parameterization**                    | Estimation vs calibration <br> (Un)Feasibility of estimation <br> Available strategies
    *Simulation: Boom-and-bust scenario*          | Irrational exuberance <br> Boom and bust cycle <br> Asset price cycle
 
 
??? info "Area 3: Macroprudential policy simulations"

    TOPIC | CONTENT
    ---|---
    **Capital-based policy tools**                | Modeling CAR-based regulation <br> Leverage regulation
    *Simulations*                                 | Introducing capital buffers <br> Estimating costs and benefits of capital-based policies <br> Varying assumptions about recapitalization, retained earnings
    **Volume-based policy tools**                 | Modeling credit caps <br> Modeling DTI, DSTI limits
    *Simulations*                                 | Introducing credit caps <br> Introducing DTI, DSTI
    **Credit gap (un)estimability**               | Feasibility of estimating financial cycle position <br> Forecasting vs scenario building
 
??? info "Area 4: Data-based simulations"

    TOPIC | CONTENT
    ---|---
    **Delta method**                              | Using data and macro forecast as inputs <br> Building scenarios of top of macro baseline <br> Using delta method in EBA-style stress-testing exercises
    *Simulation: Delta Method*                    | Creating a downside scenario on top of macro baseline <br> Introducing financial shocks on top of macro baseline
    *Simulation: Policy Interventions*            | Adding macroprudential interventions on top of macro baseline
    *Simulation: Climate risk scenarios*          | Adding macroprudential interventions on top of macro baseline
