--- 
title: In-Depth Macroprudential Modeling Workshop 2023
---

# :fontawesome-solid-pen: Workshop on Macroprudential Modeling Theoretical Basics

**We would like to thank all participants for their contributions to the workshop.**


### Format

* Lectures accompanied by simulation exercises

### Content
* Key features of macroprudential models: what features are necessary for a macroprudential model to be useful?
* GIMM in-house modeling framework: overview and derivation of key equations
* Possible alternative approaches, their pros and cons
* Discussion of key parameters, sensitivity of the results to these parameters, calibration strategies
* Overview of commonly used frameworks, their strengths and weaknesses
* Advanced topics in scenario building: role of expectations for macroprudential policy modeling
* Climate-related risks


### Benefits

Participants received the following:

* Complete modeling framework - equations, documentation, understanding of key transmission channels
* Commented codes - model files, simulation files, data files, reporting files
* Presentations


### When, where

**Dates:** November 27 - December 1, 2023, 5 day event

**Place:** Prague, Czech Republic
