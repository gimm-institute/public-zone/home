---
title: Membership
---

# :fontawesome-solid-user-lock: Membership

Membership in the Global Institute for Macroprudential Modeling (GIMM) is primarily open to government departments, public policy institutions, regulatory authorities (including central banks, financial stability boards, and treasuries), research and academic institutions, universities, and international organizations. The membership fee is determined based on the World Bank income classification of the member's country and is set at a level to cover the operational costs of GIMM.


[Download membership leaflet.](../assets/GIMM_membership_leaflet.pdf)

Full GIMM members have access to

> :fontawesome-solid-users:  All GIMM events, such as workshops and seminars
> 
> :fontawesome-solid-book:  Customized version of the GIMM modeling framework, fitted to their economy
> 
> :fontawesome-solid-compass-drafting:  Assistance in designing and implementing model-based frameworks in
> their own institution
>
> :fontawesome-solid-cube:  Assistance in designing scenarios, policy analsysis and stress tests


